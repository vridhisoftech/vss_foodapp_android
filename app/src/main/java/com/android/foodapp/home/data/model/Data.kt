package com.android.foodapp.home.data.model

data class Data(
    val viewList: List<ViewList>,
    val viewType: String,
    val label: String
)