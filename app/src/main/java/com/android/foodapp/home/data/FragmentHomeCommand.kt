package com.android.foodapp.home.data

import com.android.foodapp.home.ui.home.newsfeed.FeedResponse

interface FragmentHomeCommand {
    fun onFailure(message: String)
    fun onCategoryClick(position: Int)
    fun showToast(message: String)
    fun newsFeedList(response: FeedResponse)
    fun homeFeedList(response: FeedResponse)
}