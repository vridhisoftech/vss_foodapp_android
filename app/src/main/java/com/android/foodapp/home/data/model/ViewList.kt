package com.android.foodapp.home.data.model

data class ViewList(
    val catName: String = "",
    val imgUrl: String
)